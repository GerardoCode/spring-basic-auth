package com.atos.tflab.springtesting.repository;

import com.atos.tflab.springtesting.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findOneByUsernameIgnoreCase(String name);

}
