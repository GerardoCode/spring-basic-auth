package com.atos.tflab.springtesting.service;

import com.atos.tflab.springtesting.model.User;
import com.atos.tflab.springtesting.model.UserDetailWrapper;
import com.atos.tflab.springtesting.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findOneByUsernameIgnoreCase(username).orElseThrow(() -> new RuntimeException("User not found"));

        return new UserDetailWrapper(user);
    }
}
