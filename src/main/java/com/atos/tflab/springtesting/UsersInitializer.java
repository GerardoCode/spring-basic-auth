package com.atos.tflab.springtesting;

import com.atos.tflab.springtesting.model.Role;
import com.atos.tflab.springtesting.model.RoleType;
import com.atos.tflab.springtesting.model.User;
import com.atos.tflab.springtesting.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class UsersInitializer {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @EventListener
    public void initUser(ContextRefreshedEvent event) {

        User harold = new User();
        harold.setUsername("harold");
        harold.setPassword(passwordEncoder.encode("hide.the.pain"));
        harold.setRoles(Arrays.asList(new Role(RoleType.ROLE_USER)));

        User paco = new User();
        paco.setUsername("paco");
        paco.setPassword(passwordEncoder.encode("123"));
        paco.setRoles(Arrays.asList(new Role(RoleType.ROLE_ADMIN)));

        userRepository.save(harold);
        userRepository.save(paco);
    }

}
