package com.atos.tflab.springtesting.model;

public enum RoleType {
    ROLE_USER, ROLE_ADMIN
}
