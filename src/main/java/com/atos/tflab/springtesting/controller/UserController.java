package com.atos.tflab.springtesting.controller;

import com.atos.tflab.springtesting.model.User;
import com.atos.tflab.springtesting.model.UserDetailWrapper;
import com.atos.tflab.springtesting.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserRepository userRepository;


    @GetMapping("/me")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public User getLoggedUser() {

        return (User) ((UserDetailWrapper) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN')")
    public Iterable<User> findAllUsers() {

        return userRepository.findAll();
    }
}
